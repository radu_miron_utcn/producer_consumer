package edu.tucn.producerconsumer.simpleexample;

import edu.tucn.producerconsumer.simpleexample.consumer.Consumer;
import edu.tucn.producerconsumer.simpleexample.producer.Producer;

import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) {
        ArrayBlockingQueue<String> taskQueue = new ArrayBlockingQueue<>(100);
        new Producer(taskQueue).start();
        new Consumer(taskQueue).start();
    }
}
