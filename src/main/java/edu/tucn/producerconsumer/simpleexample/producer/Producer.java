package edu.tucn.producerconsumer.simpleexample.producer;

import java.util.Scanner;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Producer extends Thread {
    private ArrayBlockingQueue<String> taskQueue;

    public Producer(ArrayBlockingQueue<String> taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        Scanner scanner = new Scanner(System.in);

        while (true) {
            System.out.println("Input your name:");
            String name = scanner.nextLine();

            try {
                taskQueue.put(name);
            } catch (InterruptedException ignore) {
            }
        }
    }
}
