package edu.tucn.producerconsumer.simpleexample.consumer;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.stream.Collectors;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    private ArrayBlockingQueue<String> taskQueue;

    public Consumer(ArrayBlockingQueue<String> taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String name = taskQueue.take();
                String formattedName = Arrays.stream(name.split("\\s+"))
                        .map(nc -> StringUtils.capitalize(nc.toLowerCase()))
                        .collect(Collectors.joining(" "));
                Files.write(Paths.get("formatted_names.txt"), (formattedName + "\n").getBytes());
            } catch (InterruptedException e) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
