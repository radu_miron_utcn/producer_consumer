package edu.tucn.producerconsumer.dataingestion.testhttp;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws URISyntaxException, IOException, InterruptedException {
        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI("http://localhost:8081/stocks"))
                .header("Content-Type", "application/json")
                .POST(HttpRequest.BodyPublishers.ofString(
                        "{\"companyName\":\"TSLA\",\"date\":\"2022-05-05T00:00:01\",\"price\":945.75}"
                ))
                .build();

        HttpClient httpClient = HttpClient.newHttpClient();
        HttpResponse<String> response =
                httpClient.send(request, HttpResponse.BodyHandlers.ofString());

        System.out.println("response code:" + response.statusCode());
        System.out.println(response.body());
    }
}
