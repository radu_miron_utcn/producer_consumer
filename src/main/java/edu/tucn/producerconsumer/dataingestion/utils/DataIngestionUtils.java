package edu.tucn.producerconsumer.dataingestion.utils;

import com.gargoylesoftware.htmlunit.BrowserVersion;
import com.gargoylesoftware.htmlunit.WebClient;
import com.gargoylesoftware.htmlunit.html.HtmlPage;
import com.google.gson.Gson;
import org.apache.commons.io.FileUtils;
import org.dom4j.Document;
import org.dom4j.DocumentException;
import org.dom4j.Element;
import org.dom4j.io.SAXReader;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Radu Miron
 * @version 1
 */
public class DataIngestionUtils {
    private static final String BROKEN_LINE = "<meta name=\"uac-config\" content=\"{\" breakpoints\":{\"at4units\":0,\"at8units\":656,\"at12units\":976,\"at16units\":1296}};\"=\"\"/>";
    private static volatile ThreadLocal<WebClient> webClientTL = new ThreadLocal<>();

    private DataIngestionUtils() {
    }

    public static WebClient getWebClientTL() {
        if (webClientTL.get() == null) {
            WebClient webClient = new WebClient(BrowserVersion.BEST_SUPPORTED);
            webClient.getOptions().setJavaScriptEnabled(false);
            webClientTL.set(webClient);
        }

        return webClientTL.get();
    }

    public static File saveWebPage(String url, String filePath) throws IOException {
        WebClient webClient = DataIngestionUtils.getWebClientTL();
        HtmlPage page = webClient.getPage(url);
        File webPageFile = new File(filePath);

        String pageAsXml = page.asXml();

        pageAsXml = pageAsXml.replace(BROKEN_LINE, "");

        FileUtils.writeStringToFile(webPageFile, pageAsXml, StandardCharsets.UTF_8);

        return webPageFile;
    }

    public static SharePriceEntry extractSharePrice(File file) throws DocumentException {
        SAXReader reader = new SAXReader();
        Document document = reader.read(file);
        Element element = (Element) document.selectSingleNode("//*[@id=\"maincontent\"]/div[2]/div[3]/div/div[2]/h2/bg-quote");
        String company = file.getName().substring(0, file.getName().indexOf("-"));
        float price = Float.parseFloat(element.getTextTrim());
        String date = new SimpleDateFormat("yyyy-MM-dd hh:mm:ss").format(new Date());
        return new SharePriceEntry(company, date, price);
    }

    public static void saveSharePrice(SharePriceEntry sharePriceEntry) throws IOException {
        Gson gson = new Gson();
        String sharePriceJson = gson.toJson(sharePriceEntry);
        FileUtils.writeStringToFile(new File("processed-data/" + sharePriceEntry.getCompanyName() + ".dat"),
                sharePriceJson + "\n",
                StandardCharsets.UTF_8,
                true);
    }

    public static class SharePriceEntry {
        private String companyName;
        private String date;
        private float price;

        public SharePriceEntry(String companyName, String date, float price) {
            this.companyName = companyName;
            this.date = date;
            this.price = price;
        }

        public String getCompanyName() {
            return companyName;
        }

        public void setCompanyName(String companyName) {
            this.companyName = companyName;
        }

        public String getDate() {
            return date;
        }

        public void setDate(String date) {
            this.date = date;
        }

        public float getPrice() {
            return price;
        }

        public void setPrice(float price) {
            this.price = price;
        }
    }
}
