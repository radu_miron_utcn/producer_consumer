package edu.tucn.producerconsumer.dataingestion;

import edu.tucn.producerconsumer.dataingestion.consumer.Consumer;
import edu.tucn.producerconsumer.dataingestion.producer.Producer;
import org.dom4j.DocumentException;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Main {
    public static void main(String[] args) throws DocumentException {
        ArrayBlockingQueue<File> taskQueue = new ArrayBlockingQueue<>(100);
        new Producer(taskQueue).start();
        new Consumer(taskQueue).start();
    }
}
