package edu.tucn.producerconsumer.dataingestion.consumer;

import edu.tucn.producerconsumer.dataingestion.utils.DataIngestionUtils;

import java.io.File;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Consumer extends Thread {
    private ArrayBlockingQueue<File> taskQueue;

    public Consumer(ArrayBlockingQueue<File> taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                File taskFile = taskQueue.take();
                DataIngestionUtils.SharePriceEntry sharePriceEntry = DataIngestionUtils.extractSharePrice(taskFile);
                DataIngestionUtils.saveSharePrice(sharePriceEntry);
                taskFile.delete();
            } catch (InterruptedException e) {
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
