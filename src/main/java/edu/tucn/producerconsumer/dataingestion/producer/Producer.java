package edu.tucn.producerconsumer.dataingestion.producer;

import edu.tucn.producerconsumer.dataingestion.utils.DataIngestionUtils;

import java.io.File;
import java.io.IOException;
import java.util.UUID;
import java.util.concurrent.ArrayBlockingQueue;

/**
 * @author Radu Miron
 * @version 1
 */
public class Producer extends Thread {
    private static final String BASE_URL = "https://www.marketwatch.com/investing/stock/";

    private ArrayBlockingQueue<File> taskQueue;
    private String company = "TSLA";

    public Producer(ArrayBlockingQueue<File> taskQueue) {
        this.taskQueue = taskQueue;
    }

    @Override
    public void run() {
        while (true) {
            try {
                String filePath = "raw-data/" + company + "-" + UUID.randomUUID() + ".html";
                File webPageFile = DataIngestionUtils.saveWebPage(BASE_URL+company, filePath);

                taskQueue.put(webPageFile);

                Thread.sleep(60 * 1000);
            } catch (InterruptedException ignored) {
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void setCompany(String company) {
        this.company = company;
    }
}

